import router from '@system.router';
import app from '@system.app';
import prompt from '@system.prompt';
import {getApp} from '../../common.js';

export default {
    data: {
        isNetFlag: true,
        showPwd: false,
        account: {},
        wifiPwd: '', //wifi password
        isDefaultPsw: true,
        wifiFlag: false,
        wifiInfo: ''
    },
    onInit: function () {
        if (this.wifiFlag == true) {
            this.account = this.wifiInfo;
            if (this.account.hasDefaultPassword) {
                this.wifiPwd = '******';
            }
        } else {
            this.getWifiList();
        }
    },
//obtain the Wi-Fi list
    getWifiList: function () {
        getApp(this).NetConfig.getWifiList((result) => {
            if (result.code != -1 && result.data && result.data.length > 0) {
                //todo the requested Wi-Fi list can be obtained from value.data
                //todo the vendor can resolve the problem by itself or refer to the following method
                this.account = result.data[0];
                if (this.account.hasDefaultPassword) {
                    this.wifiPwd = '******';
                }
            }
        });
    },
//choose other wifi
    otherWifiClick: function () {
        router.push({
            uri: 'pages/wifi/wifi'
        });
    },
//clicking event of the cancel button
    cancelClick: function () {
        app.terminate();
    },
//clicking event of the connection button
    connectClick: function () {
        if (this.wifiPwd == '') {
            prompt.showToast({
                message: this.$t('strings.page-netconfig-pwdPlaceHolder'),
                duration: 3500
            });
            //todo in the actual development environment, return when the wifiPwd is empty.
            return;
        }
        getApp(this).ConfigParams.wifiInfo = this.account;
        if (getApp(this).ConfigParams.wifiInfo.isDefaultWifi) {
            getApp(this).ConfigParams.wifiPwd = '';
        }
        if (getApp(this).NfcInfo.sessionId != null && getApp(this).NfcInfo.sessionId != '' &&
        getApp(this).NfcInfo.sessionId != 'NAN_DEVICE_NOT_FOUND') {
            this.discoverDeviceByNan(getApp(this).NfcInfo.sessionId);
        } else {
            this.guideToLocation(true);
        }
    },
//switch to the position selection screen based on the NAN scanning result
    guideToLocation: function (softApFlag) {
        router.push({
            uri: 'pages/location/location',
            params: {
                'softApFlag': softApFlag
            }
        });
    },
//obtain the Wi-Fi password manually entered by the user
    getWifiPwd: function (e) {
        let accountPwd = e.value;
        if (accountPwd != '') {
            this.wifiPwd = accountPwd;
        }
    },
//discover NAN devices, sessionId: acquired by a touch
    discoverDeviceByNan: function (sessionId) {
        let scanInfo = {
            duration: 20,
            lockTime: 60,
            sessionId: sessionId
        };
        getApp(this).NetConfig.discoveryByNAN(scanInfo, (result) => {
            //todo the data processing can be determined by the vendor
            if (result.code == 0) {
                getApp(this).ConfigParams.deviceInfo = result.data;
                this.connectDevice();
            } else { //if the NAN cannot discover devices, use the SoftAP to configure the network
                this.guideToLocation(true);
            }
        });
    },
//connect device
    connectDevice: function () {
        let connectInfo = {
            targetDeviceId: getApp(this).ConfigParams.deviceInfo.sn,
            type: 0,
            pin: getApp(this).ConfigParams.pin,
            password: '',
            sessionId: getApp(this).ConfigParams.deviceInfo.sessionId
        };
        getApp(this).NetConfig.connectDevice(connectInfo, (result) => {
            if (result.code === 0) {
                getApp(this).ConfigParams.vendorData = result.data;
                //todo the vendor can send message to their required services
                this.configDevice();
            } else {
                //todo connect fail, the vendor processes the service logic based on its own solution
                //disconnect device
                this.disconnectDevice();
            }
        });
    },
//send the Wi-Fi account and password to configure device
    configDevice: function () {
        let netConfigInfo = {
            ssid: getApp(this).ConfigParams.wifiInfo.ssid,
            ssidPassword: getApp(this).ConfigParams.wifiPwd,
            isDefaultPassword: getApp(this).ConfigParams.wifiInfo.hasDefaultPassword,
            channel: parseInt(getApp(this).ConfigParams.wifiInfo.channel),
            sessionId: getApp(this).ConfigParams.deviceInfo.sessionId,
            type: 0,
            wifiApId: getApp(this).ConfigParams.wifiInfo.wifiApId,
            vendorData: getApp(this).ConfigParams.vendorData,
            timeout: 20
        };
        getApp(this).NetConfig.configDevice('', '', netConfigInfo, (result) => {
            if (result.code === 0 && result.data.code === 0) {
                this.guideToLocation(false);
            } else {
                //todo NAN configure fail，the vendor processes the service logic based on its own solution
                //disconnect device
                this.disconnectDevice();
            }
        });
    },
//disconnect device，this method is invoked when the configuration fails
    disconnectDevice: function () {
        let commonInfo = {
            sessionId: getApp(this).ConfigParams.deviceInfo.sessionId
        };
        getApp(this).disconnectDevice(commonInfo, () => {
        });
    }
};
