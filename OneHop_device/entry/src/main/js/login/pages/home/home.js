import router from '@system.router';

export default {
    data: {
        homes: [],
        home: '',
        softApFlag: true
    },
    onInit: function () {
        this.homes.push(this.$t('strings.page-home-one'));
        this.homes.push(this.$t('strings.page-home-two'));
        this.homes.push(this.$t('strings.page-home-three'));
        this.homes.push(this.$t('strings.page-home-four'));
    },
//select home
    homeChange: function (data) {
        this.home = this.homes[data.value];
    },
//clicking event of the confirm button
    confirmClick: function () {
        router.push({
            uri: 'pages/location/location',
            params: {
                'homeFlag': true,
                'homeInfo': this.home,
                'softApFlag': this.softApFlag
            }
        });
    },
//clicking event of the cancel button
    cancelClick: function () {
        router.back();
    }
};