import router from '@system.router';

export default {
//clicking event of the cancel button
    cancelClick: function () {
        router.back();
    },
//clicking event of the help button
    viewHelpClick: function () {
        router.push({
            uri: 'pages/help/help'
        });
    }
};