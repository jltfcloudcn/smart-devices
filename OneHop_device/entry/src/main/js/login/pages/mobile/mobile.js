import app from '@system.app';
import router from '@system.router';

const ONE_SECOND = 1000;

export default {
    data: {
        mobileNumber: '',
        inputVerificationCode: '',
        prompt: ''
    },
    onInit() {
        this.prompt = this.$t('strings.page-mobile-getVerificationCode');
    },
//todo the vendor needs to write the logic for obtaining the SMS verification code
    getVerificationCode: function () {
        let seconds = 60;
        let interval = setInterval(() => {
            if (seconds > 0) {
                seconds--;
                this.prompt = seconds;
            } else {
                this.prompt = this.$t('strings.page-mobile-getVerificationCode');
                clearInterval(interval);
            }
        }, ONE_SECOND);
    },
//obtain the SMS verification code entered by a user
    getVerificationCodeInfo: function (e) {
        let accountPwd = e.text;
        if (accountPwd != '') {
            this.inputVerificationCode = accountPwd;
        }
    },
//obtain the mobile number entered by a subscriber
    getPhoneNumer: function (e) {
        let accountPwd = e.text;
        if (accountPwd != '') {
            this.mobileNumber = accountPwd;
        }
    },
//todo button is to authorize and bind，vendors write service logic based on their own cloud protocols
    authorClick: function () {
        router.push({
            uri: 'pages/netconfig/netconfig'
        });
    },
//click cancel to close the page
    cancelClick: function () {
        app.terminate();
    }
};
