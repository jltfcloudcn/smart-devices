import router from '@system.router';

export default {
    data: {
        mobileNumber: '',
        isGetMobileNumber: true
    },
    onInit: function () {
        this.mobileNumber = this.$t('strings.mobileNumber');
    },
    changeGetMobileNumber(e) {
        this.isGetMobileNumber = e.checked;
    },
//clicking event of the cancel button
    cancelClick: function () {
        router.push({
            uri: 'pages/index/index'
        });
    },
//clicking event of the auth button
    authorClick: function () {
        if (this.isGetMobileNumber) {
            router.push({
                uri: 'pages/binding/binding'
            });
        } else {
            router.push({
                uri: 'pages/mobile/mobile'
            });
        }
    }
};