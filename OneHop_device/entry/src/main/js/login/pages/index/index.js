// @ts-ignore
import {getApp} from '../../common.js';
import app from '@system.app';
import router from '@system.router';

export default {
    data: {
        signInResult: {},
        storageData: {
            openId: ''
        },
        isGetMobileNumber: true
    },
    onInit: function () {
        //todo comment out the UI for display and do not need to be commented out in actual development.
        //        this.getNfcInfo();
        getApp(this).Product.productId = this.productId;
        getApp(this).Product.productName = this.productName;
    },
//get the NFC information obtained by touching
    getNfcInfo: function () {
        getApp(this).NfcInfo.sessionId = this.nanSessionId;
        getApp(this).NfcInfo.tagUid = this.bytesToHexString(this.tagUid).toUpperCase();
        getApp(this).NfcInfo.sn = this.sn;
    },
    bytesToHexString: function (arrBytes) {
        var str = "";
        for (var i = 0; i < arrBytes.length; i++) {
            var tmp;
            var num = arrBytes[i];
            if (num < 0) {
                tmp = (255 + num + 1).toString(16);
            } else {
                tmp = num.toString(16);
            }
            if (tmp.length == 1) {
                tmp = "0" + tmp;
            }
            str += tmp;
        }
        return str;
    },
//hw authorization icon click event
    hwLoginClick: async function() {
        //check whether a hw mobile number is obtained
        if (this.isGetMobileNumber) {
            this.goToAuthPage();
        } else {
            this.goToBindingPage();
        }
    },
//the page for obtaining mobile numbers is displayed
    goToAuthPage: function () {
        router.push({
            uri: 'pages/authorize/authorize'
        });
    },
//to the network configuration page
//todo you need to determine whether the binding is authorized. If the binding is authorized,
//todo directly jump to the network configuration page
    goToNetConfigPage: function () {
        router.push({
            uri: 'pages/netconfig/netconfig'
        });
    },
//the page for binding the mobile number is displayed
    goToBindingPage: function () {
        router.push({
            uri: 'pages/binding/binding'
        });
    },
//clicking event of the cancel button
    cancelLoginClick: async function() {
        app.terminate();
    }
};
