import router from '@system.router';

export default {
//clicking event of the cancel button
    cancelClick: function () {
        router.back();
    },
//clicking event of the config button
    configAgainClick: function () {
        router.push({
            uri: 'pages/netconfig/netconfig'
        });
    }
};