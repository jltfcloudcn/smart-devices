import router from '@system.router';

export default {
    data: {
        mobileNumber: ''
    },
    onInit: function () {
        this.mobileNumber = this.$t('strings.mobileNumber');
    },
//clicking event of the bind button
    bindClick: function () {
        this.bindMobileNumber();
        router.push({
            uri: 'pages/netconfig/netconfig'
        });
    },
//clicking event of the useOtherNumber button
    useOtherNumberClick: function () {
        router.push({
            uri: 'pages/mobile/mobile'
        });
    },
//clicking event of the cancel button
    cancelClick: function () {
        router.push({
            uri: 'pages/index/index'
        });
    },
    bindMobileNumber: function () {
        //todo handle the mobile number binding service
    }
};