import app from '@system.app';
import router from '@system.router';
import {getApp} from '../../common.js';

export default {
    data: {
        home: '',
        roomList: [],
        configSuccess: false,
        homeFlag: false,
        homeInfo: '',
        softApFlag: true
    },
    onInit: function () {
        this.home = this.$t('strings.page-home-one');
        if (this.homeFlag == true) {
            this.home = this.homeInfo;
        }
        this.configSuccess = !this.softApFlag;
        this.roomList.push({
            name: this.$t('strings.page-location-room-one'),
            color: '#DDDDDD'
        });
        this.roomList.push({
            name: this.$t('strings.page-location-room-two'),
            color: '#DDDDDD'
        });
        this.roomList.push({
            name: this.$t('strings.page-location-room-three'),
            color: '#DDDDDD'
        });
        this.roomList.push({
            name: this.$t('strings.page-location-room-four'),
            color: '#DDDDDD'
        });
    },
//clicking event of the otherHome button
    otherHomeClick: function () {
        router.push({
            uri: 'pages/home/home',
            params: {
                'softApFlag': this.softApFlag
            }
        });
    },
//select room
    roomClick: function (idx) {
        for (let i = 0; i < this.roomList.length; i++) {
            this.roomList[i].color = '#DDDDDD';
        }
        this.roomList[idx].color = '#750a59f7';
    },
//clicking event of the complete button
    completeClick: function () {
        if (this.configSuccess) {
            this.goToControl();
        } else {
            router.push({
                uri: 'pages/softap/softap'
            });
        }
    },
//clicking event of the cancel button
    cancelClick: function () {
        router.back();
    },
//the device control page is displayed
    goToControl: function () {
        let target = {
            bundleName: 'com.example.myapplication',
            abilityName: 'com.example.myapplication.ControlMainAbility',
            data: {
                sessionId: getApp(this).ConfigParams.deviceInfo.sessionId
            }
        }
        FeatureAbility.startAbility(target);
        app.terminate()
    }
};