#include <stdio.h>

#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "hi_gpio.h"
#include "hi_io.h"

#define LED_TASK_STACK_SIZE 512
#define LED_TASK_PRIO 25
#define LED_Out_GPIO 9 // for hispark_pegasus
#define LED_KZ_GPIO 5

// enum LedState g_ledState = LED_SPARK;
int USER_KZ_GPIO = 0;

static void *UserButton(const char *arg){
     USER_KZ_GPIO=!USER_KZ_GPIO;
}

static void *kzLed(const char *arg)
{   
    hi_gpio_register_isr_function(LED_KZ_GPIO, HI_INT_TYPE_EDGE, HI_GPIO_EDGE_FALL_LEVEL_LOW,
                                    UserButton, NULL);

    while (1)
    {
        IoTGpioSetOutputVal(LED_Out_GPIO, USER_KZ_GPIO);
    }

    return NULL;
}


static void LedExampleEntry(void)
{
    osThreadAttr_t attr;

    hi_gpio_init();

    
    hi_io_set_func(LED_Out_GPIO, HI_IO_FUNC_GPIO_9_GPIO);
    hi_gpio_set_dir(LED_Out_GPIO, HI_GPIO_DIR_OUT);

    hi_io_set_func(LED_KZ_GPIO, HI_IO_FUNC_GPIO_5_GPIO);
    hi_gpio_set_dir(LED_KZ_GPIO, HI_GPIO_DIR_IN);

    hi_io_set_pull(LED_KZ_GPIO, HI_IO_PULL_UP);
    
    

    attr.name = "kzLed";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = LED_TASK_STACK_SIZE;
    attr.priority = LED_TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)kzLed, NULL, &attr) == NULL) {
        printf("[LedExample] Falied to create LedTask!\n");
    }
}

SYS_RUN(LedExampleEntry);
