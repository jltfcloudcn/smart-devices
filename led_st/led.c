#include <stdio.h>
#include <unistd.h>

#include "ohos_init.h"
#include "cmsis_os2.h"
#include "iot_gpio.h"
#include "iot_watchdog.h"

#define LED_TASK_STACK_SIZE 512
#define LED_TASK_PRIO 25
#define IOT_IO_NAME_GPIO_9 9


// LED任务函数，字符串参数，任意返回值
static void *LedTask(const char *arg) {
  while (1) {
        IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_9, 0);
        usleep(1000);
        IoTGpioSetOutputVal(IOT_IO_NAME_GPIO_9, 1);
        usleep(1000);
  }
  return NULL;
}

// LED对应GPIO09，User按钮对应GPIO05
static void LedEntry(void) {
  // GPIO引脚初始化
  IoTGpioInit(IOT_IO_NAME_GPIO_9);

  // 设置GPIO09为输出
  IoTGpioSetDir(IOT_IO_NAME_GPIO_9, IOT_GPIO_DIR_OUT);
      
  // 设置线程属性
  osThreadAttr_t attr;
  attr.name = "LedTask";
  attr.attr_bits = 0U;
  attr.cb_mem = NULL;
  attr.cb_size = 0U;
  attr.stack_mem = NULL;
  attr.stack_size = LED_TASK_STACK_SIZE;
  attr.priority = LED_TASK_PRIO;

  // 开启线程，执行LED任务。开启线程，避免主线程阻塞
  if (osThreadNew((osThreadFunc_t)LedTask, NULL, &attr) == NULL) {
    printf("SunLaoTest-Fail Create");
  }
}

// 系统自带的宏，注册启动阶段自动运行代码，这里标志业务主入口是LedEntry方法。
SYS_RUN(LedEntry);
